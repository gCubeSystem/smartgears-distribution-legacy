This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Smartgears Distribution

## [v3.6.0]

- upgraded gcube-smartgears-bom


## [v3.5.0]

- upgraded gcube-smartgears-bom
- new common-smartgears version
- added resource-registry-handlers to distribution 

## [v3.4.7]

- upgraded gcube-smartgears-bom
- new common-smartgears version 

## [v3.4.6] - 2022-04-20

- added roles to ExternalService Client

## [v3.4.5] - 2022-03-29

- fixes issue [#23075]

## [v3.4.4] - 2022-03-23

- solved issue on policy check

## [v3.4.3] - 2022-01-19

- enabled policy check on smartgears
- container configuration for test added

## [v3.4.2] - 2021-11-08

- common-smartgrears library updated
    
## [v3.4.1] - 2021-06-25

- Released to have and up-to-date distribution

## [v3.4.0] - 2020-05-25

- Released to have and up-to-date distribution

## [v3.3.0]

- Accounting-lib inherited dependency has been upgraded to 4.1.0

## [v3.2.0]

- clean-container script uses proxy
 
## [v3.1.0]

- Upgraded gcube-smartgears-bom to version 2.1.0

## [v3.0.0] - 2020-11-11 

- Switched JSON management to gcube-jackson [#19283]
- Fixed distro files and pom according to new release procedure


## [v2.5.4] - 2020-06-19

- Released to have and up-to-date distribution 


## [v2-5.3] - 2020-05-07

- Released to have and up-to-date distribution


## [v2.5.1] - 2020-03-30

- Released to have and up-to-date distribution


## [v2.5.0] - 2020-02-04

- Upgraded authorization version


## [v2.4.0] - 2019-12-19

- ...


## [v2.3.0] - 2019-11-08

- Fixed distro files and pom according to new release procedure


## [v2.2.3] - 2019-05-27

- Fixed gxHTTP dependency to allow ETICs to build the component


## [v2.2.2] - 2019-02-26

- Using gcube-bom and gcube-smartgears-bom instead of maven-smartgears-bom
- Fixed changelog


## [v2.2.0] - 2018-02-15

- Changed log level from FATAL to ERROR [#9708]
- Added document-store-lib-accounting-service in place of document-store-lib-couchbase


## [v2.1.3] - 2017-10-09

- Added logback 1.11 to support Time and Size rolling of logs


## [v2.1.2] - 2017-05-02

- common-smartgears-utils re-adedd to the distribution


## [v2.1.0] - 2017-03-16

- Fixed distro files
- Added data-transfer service as part of the distribution


## [v2.0.0] - 2016-11-07

- Added new Authorization Mechanism based on token


## [v1.2.8] - 2016-07-28

- ...


## [v1.2.7] - 2016-04-08

- ...


## [v1.2.6] - 2016-02-08

- Set default log level to WARN [#2145] 
- Fixed distribution field on monitor missing when a smartgears node is upgraded [#1268]


## [v1.2.5] - 2015-12-09

- Added smartgears-distribution (this project) version on ghn profile.
- Added accounting and authorization libs


## [v1.2.4] - 2015-07-22

- Added possibility to use WEB_CONTAINER_HOME to specify the container folder. This environment variable has priority over CATALINA_HOME in the install script


## [v1.2.3] - 2015-06-18

- Stripped version from name of the included war


## [v1.2.2] - 2015-04-28

- Moved libs and war directories under target to make mvn clean properly working. The generated tar.gz remain the same


## [v1.2.1] - 2015-04-15

- smartgears-distribution-bundle version is published on ghn profile


## [v1.2.0] - 2014-04-04

- Included smartgears-utils
- Upgraded clean scripts to clean the IS
- Added symlink for shared libs


## [v1.1.0] - 2014-01-31

- JAVA 7 Release
- Included wHN manager service


## [v1.0.0] - 2013-10-24

- First Release

