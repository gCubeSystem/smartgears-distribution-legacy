FROM tomcat:8.5.47-jdk8-openjdk
  ARG nexuspath
  ARG filename
  ARG version
  WORKDIR /
  RUN curl -k  $nexuspath/$filename.tar.gz --output /smartgears-distro.tar.gz
  RUN tar zxvf /smartgears-distro.tar.gz
  RUN export CATALINA_HOME=/usr/local/tomcat
  RUN mv smartgears-distribution-$version  smartgears-distribution
  ENV GHN_HOME=./smartgears-distribution
  RUN ./smartgears-distribution/install -s tomcat    
  
